import { useState, useEffect } from "react";
import OrderList from "./OrderList";

    function Orders() {
    const [orders, setOrders]  = useState()
    const [error, setError]  = useState(null)
    const [loading, setLoading]  = useState(true)
    
    useEffect(() => {
    const Fetch = () => {
            fetch('https://gist.githubusercontent.com/Eddie901/467fdcf13bee64b796f29480c9b85cbe/raw/c1190f190027fdabfae0aa9070ef53648a3a155c/orders.json')
            .then((res) => {
                
                return res.json()
                })
                .then ((data) => {
                    setOrders(data)
                    setError(false)
                    setLoading(false)
                    }).catch((err) => {
                    setError(err)
                    setLoading(false)
            })
        }
    
        Fetch()
        }, [])






  return (
      <div>
        {loading ? (
            <p>loading...</p>
        ) : error ? (
            <p>Error: {error}</p>
        ) :  <OrderList orders={orders}/>
         }
      </div>
  );
}

export default Orders;
